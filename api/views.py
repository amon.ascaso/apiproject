from rest_framework.exceptions import ParseError
from rest_framework.decorators import action
from rest_framework.parsers import FileUploadParser, MultiPartParser, JSONParser
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from drfapp.serializers import EstudioSerializer
from drfapp.models import Estudio
from rest_framework.permissions import IsAuthenticated
from rest_framework import mixins, generics

class EstudioListView(generics.GenericAPIView, 
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.RetrieveModelMixin,
                        mixins.DestroyModelMixin):
    serializer_class = EstudioSerializer
    queryset = Estudio.objects.all()
    lookup_field = 'id'

    def get(self, request, id=None):
        if id:
            return self.retrieve(request,id)
        else:
            return self.list(request)

    def post(self, request):
        return self.create(request)

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def put(self, request, id=None):
        return self.update(request, id)

    def perform_update(self, serializer):
        return serializer.save(updated_by=self.request.user)

    def delete(self, request):
        return self.destroy(request)
