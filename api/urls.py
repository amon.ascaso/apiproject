from django.contrib import admin
from django.urls import path, include, re_path

from .views import *
from rest_framework.authtoken.views import obtain_auth_token
from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('estudios/', EstudioListView.as_view(), name='estudios'),
    path('estudios/<int:id>/', EstudioListView.as_view(), name='estudio'),
    path('api/token/', csrf_exempt(obtain_auth_token), name='obtain'),
]
