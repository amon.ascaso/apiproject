## Pasos para levantar la api:

### 1. Iniciar el entorno:

pipenv shell

### 2. Chequear si hay migraciones

django makemigrations

### 3. Hacer las migraciones

django migrate

### 4. Crear superusuario

django createsuperuser

### 5. Levantar server

django r
