from rest_framework import serializers
from .models import Estudio, Paciente, ObraSocial

class ObraSocialSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObraSocial
        fields = (
            'nombre','email','telefono'
        )

class PacienteSerializer(serializers.ModelSerializer):
    obra_social = ObraSocialSerializer(read_only=True)
    
    class Meta:
        model = Paciente
        fields = (
            '__all__'
        )

class EstudioSerializer(serializers.ModelSerializer):
    paciente = PacienteSerializer(read_only=True)
    tipo_estudio= serializers.StringRelatedField()
    diagnostico_presuntivo = serializers.StringRelatedField()
    diagnostico_presuntivo = serializers.StringRelatedField()

    class Meta:
        model = Estudio
        fields = [
            'presupuesto','comprobante','paciente','tipo_estudio','turno','diagnostico_presuntivo','muestra','informe','estado'
        ]
