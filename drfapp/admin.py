from django.contrib import admin
from drfapp.models import Estudio,ObraSocial,Paciente,TipoEstudio,DiagnosticoPresuntivo,Turno,Liquidacion,Muestra,Informe,Estado,Lote

# Register your models here.

admin.site.register(Estudio)
admin.site.register(ObraSocial)
admin.site.register(Paciente)
admin.site.register(TipoEstudio)
admin.site.register(DiagnosticoPresuntivo)
admin.site.register(Turno)
admin.site.register(Liquidacion)
admin.site.register(Muestra)
admin.site.register(Informe)
admin.site.register(Estado)
admin.site.register(Lote)