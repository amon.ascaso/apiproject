from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

# Create your models here.

class ObraSocial(models.Model):
    nombre = models.CharField(unique=True, max_length=50)
    email = models.CharField(unique=True, max_length=50)
    telefono = models.CharField(unique=True, max_length=50)

    def __str__(self):
        return self.nombre
    
class Paciente(models.Model):
    nombre = models.CharField(max_length=50, null=False, blank=False)
    apellido = models.CharField(max_length=50, null=False, blank=False)
    dni = models.CharField(unique=True, max_length=50)
    fecha_nacimiento = models.DateField()
    email = models.CharField(unique=True, max_length=100)
    telefono = models.CharField(max_length=50, null=False, blank=False)
    nombre_contacto = models.CharField(max_length=50, null=False, blank=False)
    telefono_contacto = models.CharField(max_length=50, null=False, blank=False)
    historia_clinica = models.CharField(max_length=50, null=False, blank=False)
    obra_social = models.ForeignKey(
        ObraSocial,
        on_delete=models.PROTECT,
        related_name= 'paciente'
    )

    def __str__(self):
        return f'{self.apellido} {self.nombre}'

class TipoEstudio(models.Model):
    nombre = models.CharField(unique=True, max_length=50)
    monto_extraccion = models.FloatField(null=False, blank=False)

    def __str__(self):
        return self.nombre

class DiagnosticoPresuntivo(models.Model):
    nombre = models.CharField(unique=True, max_length=50)

    def __str__(self):
        return self.nombre

class Turno(models.Model):
    fecha = models.DateTimeField(unique=True)

    def __str__(self):
        return self.fecha.strftime('%Y-%m-%d %H:%M')

class Liquidacion(models.Model):
    monto = models.FloatField(null=False, blank=False)
    liquidado = models.BooleanField(default=False)

    def __str__(self):
        return self.monto

class Muestra(models.Model):
    cantidad = models.FloatField(null=False, blank=False)
    nro_freezer = models.IntegerField(null=False, blank=False)
    liquidacion = models.ForeignKey(
        Liquidacion,
        on_delete=models.PROTECT,
        related_name= 'muestras',
        null=True
    )

    def __str__(self):
        return self.nombre

class Informe(models.Model):
    resultado = models.BooleanField(null=False, blank=False)
    detalle = models.TextField(null=False, blank=False)

    def __str__(self):
        return self.resultado

class Estado(models.Model):
    nombre = models.FloatField(null=False, blank=False)
    estado_anterior = models.ForeignKey(
        "Estado",
        on_delete=models.PROTECT,
        related_name= 'estados',
        null=True
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name='estados',
        null=True
    )
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(null=True, blank=True, editable=False)


    def __str__(self):
        return self.nombre

class Estudio(models.Model):
    paciente = models.ForeignKey(
        Paciente,
        on_delete=models.PROTECT,
        related_name= 'estudios'
    )
    presupuesto = models.FloatField(null=False, blank=False)
    tipo_estudio = models.ForeignKey(
        TipoEstudio,
        on_delete=models.PROTECT,
        related_name= 'estudios'
    )
    turno = models.ForeignKey(
        Turno,
        on_delete=models.PROTECT,
        related_name= 'estudios',
        blank=True,
        null=True
    )
    diagnostico_presuntivo = models.ForeignKey(
        DiagnosticoPresuntivo,
        on_delete=models.PROTECT,
        related_name= 'estudios'
    )
    muestra = models.ForeignKey(
        Muestra,
        on_delete=models.PROTECT,
        related_name= 'estudios',
        blank=True,
        null=True
    )
    informe = models.ForeignKey(
        Informe,
        on_delete=models.PROTECT,
        related_name= 'estudios',
        blank=True,
        null=True
    )
    estado = models.ForeignKey(
        Estado,
        on_delete=models.PROTECT,
        related_name= 'estudios',
        blank=True,
        null=True
    )
    comprobante = models.ImageField(upload_to='comprobantes/%Y/%m/%d/', max_length=255, null=True, blank=True)
    def __str__(self):
        return f'{self.paciente} - {self.tipo_estudio}'
    

class Lote(models.Model):
    estudio = models.ForeignKey(
        Estudio,
        on_delete=models.PROTECT,
        related_name= 'lotes'
    )
    lleno = models.BooleanField(default=False)
    despachado = models.BooleanField(default=False)

    def __str__(self):
        return self.pk