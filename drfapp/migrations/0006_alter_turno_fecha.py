# Generated by Django 3.2.9 on 2021-11-14 23:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drfapp', '0005_auto_20211114_2249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='turno',
            name='fecha',
            field=models.DateTimeField(unique=True),
        ),
    ]
