# Generated by Django 3.2.9 on 2021-11-20 20:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('drfapp', '0010_paciente_created_at'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='paciente',
            name='created_at',
        ),
        migrations.AddField(
            model_name='estado',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False),
        ),
        migrations.AddField(
            model_name='estado',
            name='created_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='estados', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='estado',
            name='updated_at',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
    ]
